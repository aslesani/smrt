###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
import os
import logging
import numpy as np
import tensorflow as tf
from tensorflow.python.ops import summary_ops_v2 as tfsummary
from tensorflow.python.keras.callbacks import Callback as KCallback

__all__ = [
  'TensorBoardCallback'
]

class TensorBoardCallback(KCallback):
  """Enable visualizations for TensorBoard.

  TensorBoard is a visualization tool provided with TensorFlow.

  This callback logs events for TensorBoard, including:
  * Metrics summary plots
  * Training graph visualization
  * Activation histograms
  * Sampled profiling

  If you have installed TensorFlow with pip, you should be able
  to launch TensorBoard from the command line:

  ```sh
  tensorboard --logdir=path_to_your_logs
  ```

  You can find more information about TensorBoard
  [here](https://www.tensorflow.org/get_started/summaries_and_tensorboard).

  Arguments:
      log_dir: the path of the directory where to save the log files to be
        parsed by TensorBoard.
      histogram_freq: frequency (in epochs) at which to compute activation and
        weight histograms for the layers of the model. If set to 0, histograms
        won't be computed. Validation data (or split) must be specified for
        histogram visualizations.
      write_graph: whether to visualize the graph in TensorBoard. The log file
        can become quite large when write_graph is set to True.
      write_images: whether to write model weights to visualize as image in
        TensorBoard.
      update_freq: `'batch'` or `'epoch'` or integer. When using `'batch'`,
        writes the losses and metrics to TensorBoard after each batch. The same
        applies for `'epoch'`. If using an integer, let's say `1000`, the
        callback will write the metrics and losses to TensorBoard every 1000
        samples. Note that writing too frequently to TensorBoard can slow down
        your training.
      profile_batch: Profile the batch to sample compute characteristics. By
        default, it will profile the second batch. Set profile_batch=0 to
        disable profiling. Must run in TensorFlow eager mode.

  Raises:
      ValueError: If histogram_freq is set and no validation data is provided.
  """
  def __init__(self,
               log_dir='logs',
               histogram_freq=0,
               write_graph=True,
               write_images=False,
               update_freq='epoch',
               profile_batch=2,
               **kwargs):
    super(TensorBoardCallback, self).__init__()
    self._validate_kwargs(kwargs)

    self.log_dir = log_dir
    self.histogram_freq = histogram_freq
    self.write_graph = write_graph
    self.write_images = write_images
    if update_freq == 'batch':
      self.update_freq = 1
    else:
      self.update_freq = update_freq

    self._samples_seen = 0
    self._samples_seen_at_last_write = 0
    self._current_batch = 0
    self._total_batches_seen = 0
    self._total_val_batches_seen = 0

    self._writers = []  # file writers to be closed
    self._train_writer = None  # set in `_initialize_writers`
    self._validation_writer = None  # set in `_initialize_writers`
    self._profile_batch = profile_batch
    # True when a trace is running.
    self._is_tracing = False

    # TensorBoard should only write summaries on the chief when in a
    # Multi-Worker setting.
    self._chief_worker_only = True

  def _validate_kwargs(self, kwargs):
    """Handle arguments were supported in V1."""
    if kwargs.get('write_grads', False):
      logging.warning('`write_grads` will be ignored in TensorFlow 2.0 '
                      'for the `TensorBoard` Callback.')
    if kwargs.get('embeddings_freq', False):
      logging.warning('Embeddings will be ignored in TensorFlow 2.0 '
                      'for the `TensorBoard` Callback.')
    if kwargs.get('batch_size', False):
      logging.warning('`batch_size` is no longer needed in the '
                      '`TensorBoard` Callback and will be ignored '
                      'in TensorFlow 2.0.')

    unrecognized_kwargs = set(kwargs.keys()) - {
        'write_grads', 'embeddings_freq', 'embeddings_layer_names',
        'embeddings_metadata', 'embeddings_data', 'batch_size'
    }

    # Only allow kwargs that were supported in V1.
    if unrecognized_kwargs:
      raise ValueError('Unrecognized arguments in `TensorBoard` '
                       'Callback: ' + str(unrecognized_kwargs))

  def set_model(self, model):
    """Sets Keras model and writes graph if specified."""
    self.model = model
    self._initialize_writers()

  def _close_writers(self):
    """Close all remaining open file writers owned by this callback.

    If there are no such file writers, this is a no-op.
    """
    for writer in self._writers:
      writer.close()
    del self._writers[:]

  def _initialize_writers(self):
    """Create all file writers needed and validation writers.

    This updates `self._train_writer` and `self._validation_writer`, and
    populates the `self._writers` list to be cleaned up by
    `_close_writers`.
    """
    self._close_writers()

    def create_writer(subdir):
      path = os.path.join(self.log_dir, subdir)
      return tf.summary.create_file_writer(path)

    self._train_writer = create_writer('train')
    self._writers.append(self._train_writer)
    self._validation_writer = create_writer('validation')
    self._writers.append(self._validation_writer)

  def on_train_begin(self, logs=None):
    if self._profile_batch == 1:
      tf.summary.trace_on(graph=True, profiler=True)
      self._is_tracing = True

  def on_batch_end(self, batch, logs=None):
    """Writes scalar summaries for metrics on every training batch.

    Performs profiling if current batch is in profiler_batches.
    """
    # Don't output batch_size and batch number as TensorBoard summaries
    logs = logs or {}
    self._samples_seen += logs.get('size', 1)
    samples_seen_since = self._samples_seen - self._samples_seen_at_last_write
    if self.update_freq != 'epoch' and samples_seen_since >= self.update_freq:
      self._log_metrics(logs, prefix='batch_', step=self._total_batches_seen)
      self._samples_seen_at_last_write = self._samples_seen
    self._total_batches_seen += 1
    if self._is_tracing:
      self._log_trace()
    elif (not self._is_tracing and
          self._total_batches_seen == self._profile_batch - 1):
      self._enable_trace()

  def on_epoch_end(self, epoch, logs=None):
    """Runs metrics and histogram summaries at epoch end."""
    step = epoch if self.update_freq == 'epoch' else self._samples_seen
    self._log_metrics(logs, prefix='epoch_', step=step)

    if self.histogram_freq and epoch % self.histogram_freq == 0:
      self._log_weights(epoch)

  def on_train_end(self, logs=None):
    if self._is_tracing:
      self._log_trace()
    self._close_writers()

  def _enable_trace(self):
    tf.summary.trace_on(graph=True, profiler=True)
    self._is_tracing = True

  def _log_trace(self):
    with self._train_writer.as_default():
      # TODO(b/126388999): Remove step info in the summary name.
      tf.summary.trace_export(
          name='batch_%d' % self._total_batches_seen,
          step=self._total_batches_seen,
          profiler_outdir=self.log_dir)
    self._is_tracing = False

  def _log_metrics(self, logs, prefix, step):
    """Writes metrics out as custom scalar summaries.

    Arguments:
        logs: Dict. Keys are scalar summary names, values are NumPy scalars.
        prefix: String. The prefix to apply to the scalar summary names.
        step: Int. The global step to use for TensorBoard.
    """
    if logs is None:
      logs = {}

    # Group metrics by their associated file writer. Values are lists of
    # metrics, as (name, scalar_value) pairs.
    logs_by_writer = {
        self._train_writer: [],
        self._validation_writer: [],
    }
    validation_prefix = 'val_'
    for (name, value) in logs.items():
      if name in ('batch', 'size', 'num_steps'):
        # Scrub non-metric items.
        continue
      if name.startswith(validation_prefix):
        name = name[len(validation_prefix):]
        writer = self._validation_writer
      else:
        writer = self._train_writer
      name = prefix + name  # assign batch or epoch prefix
      logs_by_writer[writer].append((name, value))

    for writer in logs_by_writer:
      with writer.as_default():
        with tfsummary.always_record_summaries():
          for (name, value) in logs_by_writer[writer]:
            tfsummary.scalar(name, value, step=step)
      writer.flush()

  def _log_weights(self, epoch):
    """Logs the weights of the Model to TensorBoard."""
    with self._train_writer.as_default():
      with tfsummary.always_record_summaries():
        for weight in self.model.weights:
          weight_name = weight.name.replace(':', '_')
          tfsummary.histogram(weight_name, weight, step=epoch)
          if self.write_images:
            self._log_weight_as_image(weight, weight_name, epoch)
    self._train_writer.flush()

  def _log_weight_as_image(self, weight, weight_name, epoch):
    """Logs a weight as a TensorBoard image."""
    w_img = np.squeeze(weight)
    shape = w_img.shape
    if len(shape) == 1:  # Bias case
      w_img = np.reshape(w_img, [1, shape[0], 1, 1])
    elif len(shape) == 2:  # Dense layer kernel case
      if shape[0] > shape[1]:
        w_img = np.transpose(w_img)
        shape = w_img.shape
      w_img = np.reshape(w_img, [1, shape[0], shape[1], 1])
    elif len(shape) == 3:  # ConvNet case
      w_img = np.reshape(w_img, [shape[0], shape[1], shape[2], 1])

    shape = w_img.shape
    # Not possible to handle 3D convnets etc.
    if len(shape) == 4 and shape[-1] in [1, 3, 4]:
      tfsummary.image(weight_name, w_img, step=epoch)
