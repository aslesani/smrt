###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""A collection of helper functions used in developing the learning model
"""

import tensorflow as tf

def tf_check_covs(gm_covs, tol=1e-5):
  """Check the covariance matrices to make sure that they are all positive
  determinant to make sure the numerical computability. If a covariance matrix
  is not positive determinant, we will add a unified minimum float to make
  it positive determinant.

  Args:
    gm_covs: Covariance matrices of Gaussian components, of shape
      `[num_gc, ndims, ndims]`.

  Returns:
    Updated covariance matrices of Gaussian components, of shape
      `[num_gc, ndims, ndims]`.
  """
  gm_shape = tf.shape(gm_covs)
  num_gc = gm_shape[0]
  ndims = gm_shape[1]
  # Check all the generalized variance for each Gaussian component is positive
  # determinant
  generalized_covs = tf.linalg.det(gm_covs)
  positive_covs = tf.greater(generalized_covs, 1e-25)
  while not tf.reduce_all(positive_covs):
    print("We found at least one covariance matrix which is not positive "
          "determinant.\r\n")
    gm_covs = tf.where(
      positive_covs,
      x=tf.zeros((num_gc, ndims, ndims), dtype=tf.float64),
      y = tf.eye(ndims, batch_shape=[num_gc], dtype=tf.float64) * tol
    ) + gm_covs
    generalized_covs = tf.linalg.det(gm_covs)
    positive_covs = tf.greater(generalized_covs, 0)
  return gm_covs


@tf.function
def tf_check_symmetric(gm_covs, tol=1e-5):
  """Check the covariance matrices to make sure that they are all symmetric
  so that Cholesky decomposition can be computed numerically.

  Args:
    gm_covs: Covariance matrices of Gaussian components, of shape
      `[num_gc, ndims, ndims]`.

  Returns:
    indices of the Gaussian components where symmetric is not guaranteed.
  """
  gm_shape = tf.shape(gm_covs)
  num_gc = gm_shape[0]
  non_sym = []
  for i in range(num_gc):
    distance = gm_covs[i, :, :] - tf.transpose(gm_covs[i, :, :])
    if not tf.reduce_all(tf.abs(distance) < tol):
      non_sym.append(i)
  return non_sym
