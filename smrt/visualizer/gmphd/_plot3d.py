import sys
import numpy as np
from mayavi.scripts import mayavi2
from mayavi import mlab
from traits.api import HasTraits, Button
from traitsui.api import View, Item


def plot3d_data_preparation(prediction=None,
                            observation=None,
                            truth=None,
                            scale_factor=10):
  """Prepare data for 3D Plot

  In this case, n_visible is 3.

  Args:
    prediction: List of predictions. TODO: Specify the prediction structure.
    observation: List of observations at each time step. The observations at
      each time step is a numpy array of shape `[num_obs, n_visible]`.
    truth: The ground truth of the state of each target. The truth is a
      dictionary with key `locations` and `targets`. The true locations at each
      time step is a numpy array of shape `[num_targets, ndims]`, and the
      targets of each step is a numpy array of shape `[num_targets,]` filled
      with corresponding target ID.
    scale_factor: The default scaling factor for target size in the plot.

  Returns:
    (:obj:`dict`): Dictionary containing a plot with lines connecting
      observation and truth in the same frame. The dictionary contains
      the following keys:

      * ``fig``: matplotlib Figure object.
      * ``ax``: matplotlib Axes.
      * ``all``: List of all :obj:`~matplotlib.lines.Line2D` drawn in
          the figure.
      * ``observation``: List of :obj:`~matplotlib.lines.Line2D`
          indexed by frame.
      * ``truth``: List of :obj:`~matplotlib.lines.Line2D`
          indexed by frame.
  """
  obs_points_list = None
  if observation is not None:
    obs_points_list = []
    # Process Observation
    plot_data = observation
    for i in range(len(plot_data)):
      observation_x = plot_data[i][:, 0]
      observation_y = plot_data[i][:, 1]
      observation_z = plot_data[i][:, 2]
      obs_points = mlab.points3d(
        observation_x, observation_y, observation_z,
        mode='cube', color=(0, 1, 0), scale_factor=scale_factor,
        opacity=0.3)
      obs_points_list.append(obs_points)
  # Process Truth if provided
  truth_points_list = None
  if truth is not None:
    truth_points_list = []
    # Process Truth
    plot_data = truth["locations"]
    for i in range(len(plot_data)):
      truth_x = plot_data[i][:, 0]
      truth_y = plot_data[i][:, 1]
      truth_z = plot_data[i][:, 2]
      truth_points = mlab.points3d(
        truth_x, truth_y, truth_z,
        mode='sphere', color=(1, 0, 0), scale_factor=scale_factor,
        opacity=0.5)
      truth_points_list.append(truth_points)
  # Process prediction if provided
  pred_points_list = None
  if prediction is not None:
    pred_points_list = []
    plot_data = prediction["locations"]
    for i in range(len(plot_data)):
      prediction_x = plot_data[i][:, 0]
      prediction_y = plot_data[i][:, 1]
      prediction_z = plot_data[i][:, 2]
      prediction_points = mlab.points3d(
        prediction_x, prediction_y, prediction_z,
        mode='cone', color=(0, 0, 1), scale_factor=scale_factor,
        opacity=0.7)
      pred_points_list.append(prediction_points)
  # Returns
  return truth_points_list, obs_points_list, pred_points_list


def plot3d_observation(observation, truth=None, ref_box=None):
  """Animate observation with 3D plot using mayavi

  Args:
    observation: List of observations at each time step. The observations at
      each time step is a numpy array of shape `[num_obs, n_visible]`.
    truth: The ground truth of the state of each target. The truth is a
      dictionary with key `locations` and `targets`. The true locations at each
      time step is a numpy array of shape `[num_targets, ndims]`, and the
      targets of each step is a numpy array of shape `[num_targets,]` filled
      with corresponding target ID.
    ref_box: The size of a reference cubic to draw as a reference, in the form
      of `[x_min, x_max, y_min, y_max, z_min, z_max]`.
  """
  global frame
  frame = 0
  if ref_box is None:
    ref_box = [-2000., 2000, -2000, 2000, -2000, 2000]
  title = "Multi-target 3D Animation"
  print("Start animation with Mayavi")

  class Controller(HasTraits):
    next_frame = Button('Next Step')
    prev_frame = Button('Previous Step')

    view = View(Item(name='next_frame'), Item(name='prev_frame'))

    def _next_frame_changed(self, value):
      global frame
      frame += 1
      print("Time step: %d" % frame)
      truth_points, obs_points, pred_points = self.data

      for i in range(frame, max(0, frame - 8), -1):
        opacity = 1. - 0.1 * (frame - i)
        if truth_points is not None:
          truth_points[i].actor.property.opacity = opacity
        if obs_points is not None:
          obs_points[i].actor.property.opacity = opacity
        if pred_points is not None:
          pred_points[i].actor.property.opacity = opacity
      mlab.draw()

    def _prev_frame_changed(self, value):
      global frame
      truth_points, obs_points, pred_points = self.data
      if truth_points is not None:
        truth_points[frame].actor.property.opacity = 0.
      if obs_points is not None:
        obs_points[frame].actor.property.opacity = 0.
      if pred_points is not None:
        pred_points[frame].actor.property.opacity = 0.

      frame -= 1
      print("Time step: %d" % frame)

      for i in range(frame, max(0, frame - 8), -1):
        opacity = 1. - 0.1 * (frame - i)
        if truth_points is not None:
          truth_points[i].actor.property.opacity = opacity
        if obs_points is not None:
          obs_points[i].actor.property.opacity = opacity
        if pred_points is not None:
          pred_points[i].actor.property.opacity = opacity
      mlab.draw()

  @mayavi2.standalone
  def mayavi_main():
    figure = mlab.figure(title)
    truth_points, obs_points, pred_points = plot3d_data_preparation(
      prediction=None,
      observation=observation,
      truth=truth
    )
    if truth_points is not None:
      for point in truth_points:
        point.actor.property.opacity = 0.
      truth_points[0].actor.property.opacity = 1.

    if obs_points is not None:
      for point in obs_points:
        point.actor.property.opacity = 0.
      obs_points[0].actor.property.opacity = 1.

    if pred_points is not None:
      for point in pred_points:
        point.actor.property.opacity = 0.
      pred_points[0].actor.property.opacity = 1.

    plot_data = (truth_points, obs_points, pred_points)
    mlab.outline(extent=ref_box)
    computation = Controller(data=plot_data, figure=figure)
    computation.edit_traits()

  mayavi_main()

# def plot3d_gmphd_track(model, grid, gm_s_list=None, gm_list_list=None,
#                        observation_list=None, prediction_list=None, truth=None,
#                        title=None, contours=4, log_plot=True):
#   """ Animate GM-PHD Filter result on 3D plot with mayavi
#
#   Args:
#       model (:obj:`pymrt.tracking.models.CVModel`):
#       grid (:obj:`numpy.ndarray`): 3D mesh generated by :func:`numpy.mgrid` or
#           :func:`numpy.meshgrid`.
#       gm_s_list (:obj:`list`): List of PHD scalars at each time step.
#       gm_list_list (:obj:`list`): List of Gaussian Mixtures at each time step.
#           If ``gm_s_list`` is None, it is used along with ``grid`` to generate
#           the PHD scalar at each time step.
#       observation_list (:obj:`list`): List of observations at each time step.
#       prediction_list (:obj:`list`): List of predictions at each time step.
#       truth (:obj:`tuple`): A tuple of truth states (by step)
#       title (:obj:`string`): Plot title.
#       contours (:obj:`int`): Number of contour surfaces to draw.
#       log_plot (:obj:`bool`): Plot ``gm_s`` in log scale.
#   """
#   global frame
#   frame = 0
#
#   if gm_s_list is None:
#     if gm_list_list is None:
#       raise ValueError("Must provide 3D sampled GM scalar gm_s or a "
#                        "Gaussian Mixture list")
#     else:
#       print('Sampling PHD in 3D space')
#       from ...tracking.utils import gm_calculate
#       gm_s_list = []
#       i = 0
#       for gm_list in gm_list_list:
#         sys.stdout.write('calculate gm_scalar for step %d' % i)
#         gm_s_list.append(gm_calculate(
#           gm_list=gm_list, grid=grid
#         ))
#         i += 1
#
#   if title is None:
#     title = 'PHD'
#
#   print('Start Plotting with Mayavi')
#
#   class Controller(HasTraits):
#     run_calculation = Button('Next Frame')
#
#     view = View(Item(name='run_calculation'))
#
#     def _run_calculation_changed(self, value):
#       # action = ThreadedAction(self.data, self.figure)
#       # action.start()
#       global frame
#       print("Update 3D plots calculation in Frame %d" % frame, end=' ')
#       truth_points, obs_points, pred_points, contour = self.data
#       if log_plot:
#         contour_s = np.log(gm_s_list[frame] + np.finfo(np.float).tiny)
#       else:
#         contour_s = gm_s_list[frame]
#       contour.mlab_source.scalars = contour_s
#       for i in range(frame, max(0, frame - 8), -1):
#         opacity = 1. - 0.1 * (frame - i)
#         truth_points[i].actor.property.opacity = opacity
#         obs_points[i].actor.property.opacity = opacity
#         pred_points[i].actor.property.opacity = opacity
#       print('done.')
#       mlab.draw()
#       frame += 1
#
#   @mayavi2.standalone
#   def mayavi_main():
#     """Example showing how to view a 3D numpy array in mayavi2.
#     """
#     figure = mlab.figure(title)
#     if log_plot:
#       contour_s = np.log(gm_s_list[frame] + np.finfo(np.float).tiny)
#     else:
#       contour_s = gm_s_list[frame]
#     contour = mlab.contour3d(
#       grid[0], grid[1], grid[2],
#       contour_s,
#       transparent=True,
#       opacity=0.5
#     )
#
#     truth_points, obs_points, pred_points = plot3d_data_preparation(
#       prediction=prediction_list,
#       observation=observation_list,
#       truth=truth
#     )
#
#     for points in truth_points:
#       points.actor.property.opacity = 0.
#
#     for points in obs_points:
#       points.actor.property.opacity = 0.
#
#     for points in pred_points:
#       points.actor.property.opacity = 0.
#
#     truth_points[0].actor.property.opacity = 1.
#     obs_points[0].actor.property.opacity = 1.
#     pred_points[0].actor.property.opacity = 1.
#
#     mlab.colorbar(contour, title='PHD', orientation='vertical')
#
#     mlab.outline(contour)
#
#     plot_data = (truth_points, obs_points, pred_points, contour)
#     computation = Controller(data=plot_data, figure=figure)
#     computation.edit_traits()
#
#   mayavi_main()
