###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""This file contains some common routine that is useful for other plotting
functions.
"""

import os
from itertools import cycle
import matplotlib as mpl
import matplotlib.image as mimg
import matplotlib.patches as mpatches
from pycasas.data import CASASSensorType
from pycasas.data import CASASDataset


def get_mpl_color_cycle():
  """Return a cycle iterator which returns the next default color defined
  in matplotlib.

  Returns:
    A cycle object that iterates the default color sequence defined in
    Matplotlib infinitely. To get the next color in the sequence, simply call
    `next` on the returned cycle object.
  """
  mpl_colors = mpl.rcParams['axes.prop_cycle'].by_key()['color']
  return cycle(mpl_colors)


def get_mpl_linestyles_cycle():
  """Return a cycle iterator which returns a new linestyles.

  Returns:
    A cycle object that iterates the line style sequence. To get the next
    line style in the sequence, simply call `next` on the returned cycle object.
  """
  return cycle([
    '-', # Solid
    '--', # Dashed
    '-.', # Dash-dotted
    ':', # Dotted
    (8, 2, 1, 2, 1, 2) # Dash with two dots "- . ."
  ])


def prepare_floorplan(dataset, categories=None):
  """Prepare the CASAS smart home floorplan

  This internal function generates a dictionary of elements to be ploted
  with matplotlib.
  The return is a dictionary composed of following keys:

  * ``img``: floor plan image loaded in :obj:`mimg` class for plotting
  * ``width``: actual width of the image
  * ``height``: actual height of the image
  * ``sensor_centers``: list of the centers to plot each sensor text.
  * ``sensor_boxes``: list of rectangles to show the location of sensor
    (:obj:`patches.Rectangle`).
  * ``sensor_texts``: list of sensor names

  Returns:
      :obj:`dict`: A dictionary contains all the pieces needed to draw
          the floorplan
  """
  assert(isinstance(dataset, CASASDataset))
  floorplan_dict = {}
  img = mimg.imread(os.path.join(
    dataset.site.directory.replace('\\', '/'), dataset.site.data_dict['floorplan'])
  )
  img_x = img.shape[1]
  img_y = img.shape[0]
  # Create Sensor List/Patches
  sensor_boxes = {}
  sensor_texts = {}
  sensor_centers = {}
  sensor_dict = {}
  # Check Bias
  for sensor in dataset.sensor_list:
    loc_x = sensor['locX'] * img_x
    loc_y = sensor['locY'] * img_y
    width_x = 0.01 * img_x
    width_y = 0.01 * img_y
    sensor_category = \
      CASASSensorType.get_best_category_for_sensor(sensor['types'])
    if categories is not None and sensor_category not in categories:
      continue
    sensor_color = CASASSensorType.get_category_color(sensor_category)
    sensor_boxes[sensor['name']] = \
      mpatches.Rectangle((loc_x, loc_y),
                         width_x, width_y,
                         edgecolor='grey',
                         facecolor=sensor_color,
                         linewidth=1,
                         zorder=2)
    sensor_texts[sensor['name']] = (loc_x,
                                    loc_y,
                                    sensor['name'])
    sensor_centers[sensor['name']] = (loc_x, loc_y)
  # Populate dictionary
  floorplan_dict['img'] = img
  floorplan_dict['width'] = img_x
  floorplan_dict['height'] = img_y
  floorplan_dict['sensor_centers'] = sensor_centers
  floorplan_dict['sensor_boxes'] = sensor_boxes
  floorplan_dict['sensor_texts'] = sensor_texts
  return floorplan_dict