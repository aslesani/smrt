# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
"""Linear Multi-Target Gaussian Mixture Model"""
import numpy as np
import scipy.stats
from scipy.spatial import distance

__all__ = [
  "LinearMTGM_Model",
  "CvMTGM_Model",
  "LinearMTGM_ID_Model",
  "CvMTGM_ID_Model"
]

class LinearMTGM_Model:
  """Linear Multi-target Gaussian Mixture Model

  The linear multi-target Gaussian mixture model is implemented with numpy
  packages. It serves as a reference for the corresponding training model
  implemented with tensorflow.

  Args:
    n_visible: Dimension of visible state.
    n_hidden: Dimension of hidden state.
    birth: A weights, means and covariance matrices three-tuple defining the
      Gaussian components of birth PHD.
    p_d: Detection probability.
    p_s: Survival probability.
    F: Linear multiplier of the dynamic model, of shape `[ndims, ndims]`.
    Q: Covariance matrices of the linear Gaussian dynamic model `[ndims, ndims]`.
    R: Covariance matrices of the linear Gaussian measurement model, of shape
      `[n_visible, n_visible]`.
    clutter_lam: Lambda of Poisson distribution.
    clutter_c: Uniform probability density of the clutter process.
  """
  def __init__(self,
               n_visible,
               n_hidden,
               birth,
               p_d=0.90,
               p_s=0.99,
               F=None,
               Q=None,
               R=None,
               clutter_lam=1.,
               clutter_c=0.01):
    self.ndims = n_visible + n_hidden
    self.n_visible = n_visible
    self.n_hidden = n_hidden
    self.p_d = p_d
    self.p_s = p_s
    self.F = F
    self.Q = Q
    self.H = np.block([
      np.eye(self.n_visible, dtype=np.float),
      np.zeros((self.n_visible, self.n_hidden), dtype=np.float)
    ])
    self.R = R
    self.birth_weights = birth[0]
    self.birth_means = birth[1]
    self.birth_covs = birth[2]
    self.clutter_lam = clutter_lam
    self.clutter_c = clutter_c

  @property
  def F(self):
    return self._F

  @F.setter
  def F(self, value):
    if value is None:
      self._F = np.block([
        np.zeros((self.ndims, self.n_visible), dtype=np.float),
        np.eye(self.ndims, self.n_hidden, dtype=np.float)
      ])
    else:
      if value.shape == (self.ndims, self.ndims):
        self._F = value
      else:
        raise ValueError(
          "The linear multiplier F of dynamic model is of shape %s while shape"
          "[%d, %d] is required." % (value.shape, self.ndims, self.ndims)
        )

  @property
  def Q(self):
    return self._Q

  @Q.setter
  def Q(self, value):
    if value is None:
      self._Q = np.eye(self.ndims, dtype=np.float)
    elif value.shape == (self.ndims, self.ndims):
      self._Q = value
    else:
      raise ValueError(
        "The covariance matrix of linear Gaussian dyanmic model provided is of "
        "shape %s while shape [%d, %d] is required." % (
          value.shape, self.ndims, self.ndims
        )
      )

  @property
  def R(self):
    return self._R

  @R.setter
  def R(self, value):
    if value is None:
      self._R = np.eye(self.n_visible, dtype=np.float)
    elif value.shape == (self.n_visible, self.n_visible):
      self._R = value
    else:
      raise ValueError(
        "The covariance matrix of linear Gaussian measurement model provided is"
        " of shape %s while shape [%d, %d] is required." % (
          value.shape, self.n_visible, self.n_visible
        )
      )

  def call(self, inputs, prunning=None):
    """GM-PHD Forward Pass

    Based on the posterior multi-target PHD (in the form of Gaussian Mixture)
    of previous time step (after the corrector), predict the multi-target PHD
    of current time step and correct the prediction based on the current
    observations.

    - gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
    - gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
    - gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
    - observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.

    Args:
      inputs: A list of tensors encodes the weights, means and covariance
        matrix of PHD in the form of a Gaussian mixture and the observations at
        current time step
      prunning: Whether the Gaussian mixture is prunned according to a merge
        threshold and maximum size of Gaussian components.

    Returns:
      A three tuple composed of weights, means and covariance matrices of the
      posterior multi-target PHD of current time step in the form of Gaussian
      mixture.
    """
    # Acquire the variables for GM-PHD of previous time step
    gm_weights = inputs[0]
    gm_means = inputs[1]
    gm_covs = inputs[2]
    # Get set of observations
    observations = inputs[3]
    appended_weights, appended_means, appended_covs = self.gm_append_birth(
      gm_weights=gm_weights,
      gm_means=gm_means,
      gm_covs=gm_covs
    )
    corrected_weights, corrected_means, corrected_covs = self.gm_corrector(
      gm_weights=appended_weights,
      gm_means=appended_means,
      gm_covs=appended_covs,
      observations=observations
    )
    predicted_weights, predicted_means, predicted_covs = self.gm_predictor(
      gm_weights=corrected_weights,
      gm_means=corrected_means,
      gm_covs=corrected_covs
    )
    return predicted_weights, predicted_means, predicted_covs

  def gm_predictor(self, gm_weights, gm_means, gm_covs):
    gm_length = gm_weights.shape[0]
    predictor_weights = gm_weights * self.p_s
    predictor_means = np.zeros((gm_length, self.ndims), dtype=np.float)
    for i in range(gm_length):
      predictor_means[i, :] = np.dot(self.F, gm_means[i, :])
    predictor_covs = np.zeros(
      (gm_length, self.ndims, self.ndims), dtype=np.float
    )
    for i in range(gm_length):
      predictor_covs[i, :, :] = np.dot(
        self.F, np.dot(gm_covs[i, :, :], self.F.T)
      )
    return predictor_weights, predictor_means, predictor_covs

  def gm_append_birth(self, gm_weights, gm_means, gm_covs):
    return np.concatenate([gm_weights, self.birth_weights], axis=0), \
      np.concatenate([gm_means, self.birth_means], axis=0), \
      np.concatenate([gm_covs, self.birth_covs], axis=0)

  def gm_corrector(self, gm_weights, gm_means, gm_covs, observations):
    # Undetected GMs
    undetected_weights = (1-self.p_d) * gm_weights
    undetected_means = np.array(gm_means)
    undetected_covs = np.array(gm_covs)
    # Corrected GMs
    hm = self._corrector_compute_hm(gm_means=gm_means)
    r_hph = self._corrector_compute_r_hph(gm_covs=gm_covs)
    K = self._corrector_compute_k(gm_covs=gm_covs, r_hph=r_hph)
    qz = self._corrector_compute_qz(
      observations=observations, hm=hm, r_hph=r_hph
    )
    wq = self._corrector_compute_wq(gm_weights=gm_weights, qz=qz)
    w_den = self._corrector_compute_weights_denominator(wq=wq)
    corrected_weights = self._corrector_compute_weights(
      w_den=w_den, wq=wq
    ).reshape([-1])
    corrected_means = self._corrector_compute_means(
      gm_means=gm_means, observations=observations, K=K, hm=hm
    ).reshape([-1, self.ndims])
    corrected_covs = self._corrector_compute_covs(
      gm_covs=gm_covs, K=K, n_obs=observations.shape[0]
    ).reshape([-1, self.ndims, self.ndims])
    return np.concatenate([undetected_weights, corrected_weights], axis=0), \
      np.concatenate([undetected_means, corrected_means], axis=0), \
      np.concatenate([undetected_covs, corrected_covs], axis=0)

  def _corrector_compute_weights_denominator(self, wq):
    """Compute the denominator of weights in the posterior PHD after the GM-PHD
    corrector.

    Args:
      wq: The $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight
        calculation, of shape `[num_obs, num_gc]`.

    Returns:
      The denominator of weights in the posterior PHD after the GM-PHD
      corrector, of shape `[num_obs,]`.
    """
    w_den = self.clutter_lam * self.clutter_c + self.p_d * np.sum(
      wq, axis=1
    )
    return w_den

  def _corrector_compute_weights(self, w_den, wq):
    """Compute the weights of the Gaussian components in the posterior PHD after
    the corrector

    Args:
      w_den: denominator of the weights, of shape `[num_obs,]`.
      wq: The $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight
        calculation, of shape `[num_obs, num_gc]`.

    Returns:
      the weights of the Gaussian components in the posterior PHD after
      the corrector, of shape `[num_obs, num_gc]`.
    """
    return wq / w_den[:, np.newaxis]

  def _corrector_compute_wq(self, gm_weights, qz):
    """Compute the $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector
    weight calculation.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      qz: The $q^{(j)}(z)$ term in the GM-PHD corrector, of shape
        `[num_obs, num_gc]`

    Returns:
      the $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight calculation,
      of shape `[num_obs, num_gc]`.
    """
    z_length = qz.shape[0]
    gc_length = gm_weights.shape[0]
    wq = np.zeros((z_length, gc_length), dtype=np.float)
    for j in range(gc_length):
      wq[:, j] = gm_weights[j] * qz[:, j]
    return wq

  def _corrector_compute_qz(self, observations, hm, r_hph):
    """Compute the $q^{(j)}(z)$ term in the GM-PHD corrector.

    :math:`q_k^{(j)}(z) = \mathcal{N}(z; Hm^{(j)}, R+HP^{(j)}H^T)`

    Args:
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.
      hm: Term Hm in the GM-PHD corrector, of shape `[num_gc, n_visible]`.
      r_hph: (R + HPH^T) term of shape `[num_gc, n_visible, n_visible]`.

    Returns:
      The $q^{(j)}(z)$ term in the GM-PHD corrector, of shape
      `[num_obs, num_gc]`
    """
    z_length = observations.shape[0]
    gc_length = hm.shape[0]
    qz = np.zeros((z_length, gc_length), dtype=np.float)
    for j in range(gc_length):
      for i in range(z_length):
        qz[i, j] = scipy.stats.multivariate_normal.pdf(
          x=observations[i, :], mean=hm[j, :], cov=r_hph[j, :, :]
        )
    return qz

  def _corrector_compute_means(self, gm_means, observations, K, hm):
    """Compute the means of the Gaussian components in the posterior
    GM-PHD after the corrector.

    :math:`m_k^{(j)}(z) = m^{(j)} + K(z - Hm^{(j)})`

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`.
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.
      K: Term K in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`
      hm: Term Hm in the GM-PHD corrector, of shape `[num_gc, n_visible]`.

    Returns:
      The means of the Gaussian components in the posterior GM-PHD after
      the corrector, of shape `[num_obs, num_gc, ndims]`.
    """
    z_length = observations.shape[0]
    gc_length = gm_means.shape[0]
    means = np.zeros((z_length, gc_length, self.ndims), dtype=np.float)
    for j in range(gc_length):
      for i in range(z_length):
        means[i, j, :] = gm_means[j, :] + np.dot(
          K[j, :, :], (observations[i, :] - hm[j, :])
        )
    return means

  def _corrector_compute_covs(self, gm_covs, K, n_obs):
    """Compute the covariance matrices for the Gaussian components in the
    posterior GM-PHD after the corrector

    :math:`P_k^{(j)} = [I - K^{(j)} H] P^{j}`

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      K: Term K in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`
      n_obs: Number of observations at current time step.

    Returns:
      Corrected covariance matrics of shape `[num_gc, ndims, ndims]`
    """
    gc_length = gm_covs.shape[0]
    covs = np.zeros((n_obs, gc_length, self.ndims, self.ndims), dtype=np.float)
    for j in range(gc_length):
      covs[:, j, :, :] = np.dot(
        np.eye(self.ndims, dtype=np.float) - np.dot(K[j, :, :], self.H),
        gm_covs[j, :, :]
      )
    return covs

  def _corrector_compute_hm(self, gm_means):
    """Compute Hm term in the GM-PHD corrector

    :math:`Hm^{j}`

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`

    Returns:
      Hm term in the GM-PHD corrector, of shape `[num_gc, n_visible]`.
    """
    gm_length = gm_means.shape[0]
    hm = np.zeros((gm_length, self.n_visible))
    for j in range(gm_length):
      hm[j, :] = np.dot(self.H, gm_means[j, :])
    return hm

  def _corrector_compute_k(self, gm_covs, r_hph):
    """Compute term K in the GM-PHD corrector

    :math:`K^{(j)}=P^{(j)}H^T(HP^{(j)}H^T + R)^{-1}`

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      r_hph: (R + HPH^T) term of shape `[num_gc, n_visible, n_visible]`.

    Returns:
      Term $K$ in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`.
    """
    gc_length = gm_covs.shape[0]
    K = np.zeros((gc_length, self.ndims, self.n_visible), dtype=np.float)
    for j in range(gc_length):
      K[j, :, :] = np.dot(
        gm_covs[j, :, :],
        np.dot(self.H.T, np.linalg.inv(r_hph[j, :, :]))
      )
    return K

  def _corrector_compute_r_hph(self, gm_covs):
    """Compute (R + HPH^T) term in the GM-PHD corrector

    :math:`H_k P_{k|k-1}^{(j)}H_k^T + R_{k}`

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.

    Returns:
      (R + HPH^T) calculated based on each Gaussian components, of shape
      `[num_gc, n_visible, n_visible]`.
    """
    gc_length = gm_covs.shape[0]
    r_hph = np.zeros(
      (gc_length, self.n_visible, self.n_visible), dtype=np.float
    )
    for j in range(gc_length):
      r_hph[j, :, :] = self.R + np.dot(
        self.H, np.dot(gm_covs[j, :, :], self.H.T)
      )
    return r_hph

  def gm_truncate(self, gm_weights, gm_means, gm_covs, Jmax):
    """Truncate the list of Gaussian components based on weights. Only a max
    number of `Jmax` Gaussian components is returned.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      Jmax: Maximum number of Gaussian components to keep in propagation.

    Returns:
      A three tuple composed of weights, means and covariance matrices of the
      truncated Guassian components.
    """
    gc_length = gm_weights.shape[0]
    if gc_length <= Jmax:
      return gm_weights, gm_means, gm_covs
    indices = np.argsort(gm_weights)[::-1]
    truncated_indices = indices[:Jmax]
    return gm_weights[truncated_indices], gm_means[truncated_indices, :], \
      gm_covs[truncated_indices, :, :]

  def gm_merge(self, gm_weights, gm_means, gm_covs, distance_threshold):
    """Reduce the number of Gaussian components by merging components located
    within a distance threshold.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      distance_threshold: Distance threshold to decide whether two Gaussian
        components should merge.

    Returns:
      A three tuple composed of weights, means and covariance matrices of the
      merged Guassian components.
    """
    # Sort the Gaussian components according to their weights
    gm_length = gm_weights.shape[0]
    indices = np.argsort(gm_weights)
    # Placeholder for new Gaussian components
    gm_merged_weights = np.zeros_like(gm_weights, dtype=np.float)
    gm_merged_means = np.zeros_like(gm_means, dtype=np.float)
    gm_merged_covs = np.zeros_like(gm_covs, dtype=np.float)
    num_gm_merged = 0
    merged_indices = set()
    # Start merge
    for i in range(gm_length):
      i_index = indices[i]
      if i_index in merged_indices:
        continue
      merge_list = []
      weight = gm_weights[i_index]
      mean = gm_means[i_index, :]
      cov = gm_covs[i_index, :, :]
      for j in range(i + 1, gm_length):
        j_index = indices[j]
        if j_index in merged_indices:
          continue
        # Check if merge needed
        gm_j_means = gm_means[j_index, :]
        mean_diff = mean - gm_j_means
        distance = np.matmul(
          mean_diff.T, np.matmul(np.linalg.inv(cov), mean_diff.T)
        )
        if distance <= distance_threshold:
          merge_list.append(j_index)
      # Merge the models
      new_weight = weight
      new_mean = mean * weight
      new_cov = cov * weight
      for j_index in merge_list:
        new_weight = new_weight + gm_weights[j_index]
        new_mean = new_mean + gm_means[j_index, :] * gm_weights[j_index]
        mean_diff = mean - gm_means[j_index, :]
        new_cov = new_cov + gm_weights[j_index] * np.matmul(
          mean_diff, mean_diff.T
        )
      new_mean = new_mean / new_weight
      new_cov = new_cov / new_weight
      gm_merged_weights[num_gm_merged] = new_weight
      gm_merged_means[num_gm_merged, :] = new_mean
      gm_merged_covs[num_gm_merged, :, :] = new_cov
      # Housekeeping tasks
      merged_indices = merged_indices.union(set(merge_list))
      num_gm_merged += 1
    # Return the merged list
    return gm_merged_weights[:num_gm_merged], \
      gm_merged_means[:num_gm_merged, :], \
      gm_merged_covs[:num_gm_merged, :, :]

  def estimator(self, gm_weights, gm_means, gm_covs, threshold=0.5):
    """Estimator the location of each target based on Gaussian component

    This is a simple estimator generally applied to synthetic data to get the
    location of each target by looking at the weights of each Gaussian component
    in the posterior PHD. If the weight is greater than 0.5, the mean of the
    corresponding Gaussian component is the estimated location of the target.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      threshold: The threshold for a Gaussian component to be considered
        representing a actual target.

    Returns:
      A two tuple consisting the detected target location and the probability of
      the existence of the corresponding target.
    """
    gm_indices = np.where(gm_weights > 0.5)
    return gm_means[gm_indices, :], gm_weights[gm_indices]

  def generate_synthetic_data(self,
                              target_birth_steps,
                              target_birth_locations,
                              target_death_steps,
                              simulation_length,
                              clutter_spatial_sample_fn,
                              bounds=None):
    """Generate synthetic data based on the model specified.

    Args:
      target_birth_steps: The time step of the birth of each target.
      target_birth_locations: The spacial location of the birth of each target.
      target_death_steps: The time step of the death of each target.
      simulation_length: The total length of the generated synthetic data.
      clutter_spatial_sample_fn: The function callback to generate noisy
        observations.
      bounds: A list of boundary values specifying the boundary of the world.
        If the location of a target exceeds the bound, it will enter from the
        opposite side of the space. The boundary value is ordered as a numpy
        array of shape `[n_visible, 2]` where the first column is the lower
        boundary and the second column is the upper bound. The noise
        observations generated by the clutter process is not affected by the
        bounds.

    Returns:
      A two tuple of ground truth and noisy observations. The ground truth is
      a dictionary object containing a list of target locations (key
      "locations"), and the corresponding target ID (key "targets"). The
      observation is a list of numpy array each details the sensor observations
      at corresponding time step.
    """
    num_targets = len(target_birth_steps)
    truth_targets = []
    for s in range(simulation_length):
      truth_targets.append([])
      for t_id in range(num_targets):
        if target_birth_steps[t_id] <= s <= target_death_steps[t_id]:
          truth_targets[s].append(t_id)
    # Target Location
    target_prev_locations = np.full(
      (num_targets, self.ndims), np.inf, dtype=np.float
    )
    # Create Poisson Distribution Class
    truth_locations = []
    observations = []
    for s in range(simulation_length):
      truth_locations.append(np.zeros(
        (len(truth_targets[s]), self.ndims), dtype=np.float
      ))
      num_clutter_points = np.random.poisson(self.clutter_lam)
      target_detected = np.random.binomial(1, self.p_d, len(truth_targets[s]))
      num_detected_targets = np.sum(target_detected)
      num_obs = int(num_clutter_points + num_detected_targets)
      observations.append(np.zeros(
        (num_obs, self.n_visible), dtype=np.float
      ))
      obs_index = 0
      for index, t_id in enumerate(truth_targets[s]):
        # Get target true state
        if target_prev_locations[t_id, 0] == np.inf:
          truth_locations[s][index, :] = target_birth_locations[t_id]
        else:
          truth_locations[s][index, :] = self._synthesize_state(
            target_prev_locations[t_id, :]
          )
        if bounds is not None:
          for d in range(self.n_visible):
            while bounds[d, 0] > truth_locations[s][index, d]:
              truth_locations[s][index, d] += (bounds[d, 1] - bounds[d, 0])
            while bounds[d, 1] < truth_locations[s][index, d]:
              truth_locations[s][index, d] -= (bounds[d, 1] - bounds[d, 0])
        target_prev_locations[t_id, :] = truth_locations[s][index, :]
        # Add observation
        if target_detected[index] == 1:
          observations[s][obs_index, :] = self._synthesize_observation(
            truth_locations[s][index, :]
          )
          if bounds is not None:
            for d in range(self.n_visible):
              while bounds[d, 0] > observations[s][obs_index, d]:
                observations[s][obs_index, d] += (bounds[d, 1] - bounds[d, 0])
              while bounds[d, 1] < observations[s][obs_index, d]:
                observations[s][obs_index, d] -= (bounds[d, 1] - bounds[d, 0])
          obs_index += 1
      while obs_index < num_obs:
        observations[s][obs_index, :] = clutter_spatial_sample_fn()
        obs_index += 1
    # End generation
    return {
      "targets": truth_targets,
      "locations": truth_locations
    }, observations

  def _synthesize_observation(self, state):
    """Add measurement noise to the observation

    Args:
      state: previous state of the target, of shape `[ndims,]`.

    Returns:
      target measurement, of shape `[n_visible,]`.
    """
    measurement = np.matmul(self.H, state) + np.random.multivariate_normal(
      mean=np.zeros((self.n_visible,), dtype=np.float),
      cov=self.R
    )
    return measurement

  def _synthesize_state(self, state):
    """Synthesize the next state of the target.

    Args:
      state: previous state of the target, of shape `[ndims,]`.

    Returns:
      target state at next time step, of shape `[ndims,]`.
    """
    next_state = np.matmul(self.F, state) + np.random.multivariate_normal(
      mean=np.zeros((self.ndims, ), dtype=np.float),
      cov=self.Q
    )
    return next_state


class CvMTGM_Model(LinearMTGM_Model):
  """Constant Velocity Multi-target Gaussian Mixture Model

  Args:
    n_visible: Dimension of visible state.
    v_stddev: Standard deviation of velocity.
    m_stddev: Standard deviation of sensor measurement.
    birth: A weights, means and covariance matrices three-tuple defining the
      Gaussian components of birth PHD.
    p_d: Detection probability.
    p_s: Survival probability.
    clutter_lam: Lambda of Poisson distribution.
    clutter_c: Uniform probability density of the clutter process.
  """
  def __init__(self,
               n_visible,
               v_stddev,
               m_stddev,
               birth,
               p_d=0.90,
               p_s=0.99,
               clutter_lam=1.,
               clutter_c=0.01):
    F = np.block([[
      np.eye(n_visible, dtype=np.float),
      np.eye(n_visible, dtype=np.float)
    ],[
      np.zeros((n_visible, n_visible), dtype=np.float),
      np.eye(n_visible, dtype=np.float)
    ]])
    G = np.block([
      [0.5 * np.eye(n_visible, dtype=np.float)],
      [np.eye(n_visible, dtype=np.float)]
    ])
    Q = np.matmul(
      G, np.matmul(np.eye(n_visible, dtype=np.float) * (v_stddev ** 2), G.T)
    )
    # Q = np.block([
    #   np.matmul(
    #     G, np.eye(n_visible, dtype=np.float) * (v_stddev ** 2)
    #   ),
    #   np.block([
    #     [np.zeros((n_visible, n_visible), dtype=np.float)],
    #     [np.eye(n_visible, dtype=np.float) * (v_stddev ** 2)]
    #   ])
    # ])
    R = np.eye(n_visible, dtype=np.float) * (m_stddev ** 2)
    super(CvMTGM_Model, self).__init__(
      n_hidden=n_visible,
      n_visible=n_visible,
      birth=birth,
      p_d=p_d,
      p_s=p_s,
      F=F,
      Q=Q,
      R=R,
      clutter_lam=clutter_lam,
      clutter_c=clutter_c
    )


class LinearMTGM_ID_Model(LinearMTGM_Model):
  """Linear Multi-target Gaussian Mixture Model with Track ID

  The linear multi-target Gaussian mixture model is implemented with numpy
  packages. It serves as a reference for the corresponding training model
  implemented with tensorflow.

  Args:
    n_visible: Dimension of visible state.
    n_hidden: Dimension of hidden state.
    birth: A weights, means and covariance matrices three-tuple defining the
      Gaussian components of birth PHD.
    p_d: Detection probability.
    p_s: Survival probability.
    F: Linear multiplier of the dynamic model, of shape `[ndims+1, ndims+1]`.
    Q: Covariance matrices of the linear Gaussian dynamic model `[ndims+1, ndims+1]`.
    R: Covariance matrices of the linear Gaussian measurement model, of shape
      `[n_visible, n_visible]`.
    clutter_lam: Lambda of Poisson distribution.
    clutter_c: Uniform probability density of the clutter process.
  """
  def __init__(self,
               n_visible,
               n_hidden,
               birth,
               p_d=0.90,
               p_s=0.99,
               F=None,
               Q=None,
               R=None,
               clutter_lam=1.,
               clutter_c=0.01):
    super(LinearMTGM_ID_Model, self).__init__(
      n_visible=n_visible,
      n_hidden=n_hidden,
      birth=birth,
      p_d=p_d,
      p_s=p_s,
      F=F,
      Q=Q,
      R=R,
      clutter_lam=clutter_lam,
      clutter_c=clutter_c
    )
    # Add track ID field to state dimensions
    self.ndims = n_visible + n_hidden + 1
    # Extending linear multiplier of measurement model with extra column for
    # track ID
    self.H = np.block([
      np.eye(self.n_visible, dtype=np.float),
      np.zeros((self.n_visible, self.n_hidden + 1), dtype=np.float)
    ])
    # Both birth and covariance matrices need to be extended with extra
    # dimension for track ID
    if self.birth_means.shape[1] != self.ndims:
      self.birth_means = np.block([
        self.birth_means, np.zeros((self.birth_means.shape[0], 1))
      ])
    if self.birth_covs.shape[1:] != (self.ndims, self.ndims):
      self.birth_covs = np.block([
        [self.birth_covs,
         np.zeros((self.birth_covs.shape[0], self.ndims - 1, 1))],
        [np.zeros((self.birth_covs.shape[0], 1, self.ndims - 1)),
         np.ones((self.birth_covs.shape[0], 1, 1))]
      ])
    self.next_track_id = 1

  @property
  def F(self):
    return self._F

  @F.setter
  def F(self, value):
    n_space_dims = self.n_visible + self.n_hidden
    if value is None:
      self._F = np.block([
        np.zeros((n_space_dims, self.n_visible), dtype=np.float),
        np.eye(n_space_dims + 1, self.n_hidden + 1, dtype=np.float)
      ])
    else:
      if value.shape == (n_space_dims, n_space_dims):
        # Extending linear multiplier with extra dimension for track ID
        self._F = np.block([
          [value, np.zeros((n_space_dims, 1))],
          [np.zeros((1, n_space_dims)), 1]
        ])
      elif value.shape == (n_space_dims+1, n_space_dims+1):
        self._F = value
      else:
        raise ValueError(
          "The linear multiplier F of dynamic model is of shape %s while shape"
          "[%d, %d] is required." % (value.shape, self.ndims, self.ndims)
        )

  @property
  def Q(self):
    return self._Q

  @Q.setter
  def Q(self, value):
    n_space_dims = self.n_visible + self.n_hidden
    if value is None:
      self._Q = np.eye(n_space_dims + 1, dtype=np.float)
    elif value.shape == (n_space_dims, n_space_dims):
      # Extending covariance matrices with extra dimension for track ID
      self._Q = np.block([
        [value, np.zeros((n_space_dims, 1))],
        [np.zeros((1, n_space_dims)), 1]
      ])
    elif value.shape == (n_space_dims+1, n_space_dims+1):
      self._Q = value
    else:
      raise ValueError(
        "The covariance matrix of linear Gaussian dyanmic model provided is of "
        "shape %s while shape [%d, %d] is required." % (
          value.shape, n_space_dims, n_space_dims
        )
      )

  def gm_id_cluster(self, gm_weights, gm_means, gm_covs):
    """GM-PHD State estimator using a clustering algorithm

    In cases where the error is higher and no single GM has a weight that goes
    beyond 0.5, cluster algorithm is needed to assign track ID to each Gaussian
    components in the posterior PHD.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims+1]`
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims+1, ndims+1]`.

    Returns:
      A three tuple composed of weights, means and covariance matrices of the
      Gaussian components with track ID assigned.
    """
    num_gc = gm_weights.shape[0]
    # A dict indexed by the track ID targeting a list of indices of Gaussian
    # components associated with the track
    track_gms_dict = {}
    for i in range(num_gc):
      track_id = int(round(gm_means[i, -1]))
      if track_id not in track_gms_dict:
        track_gms_dict[track_id] = [i]
      else:
        track_gms_dict[track_id].append(i)
    # Calculate the weights and determine if it needs clustering
    for track_id in track_gms_dict:
      cardinality = 0.
      for i in track_gms_dict[track_id]:
        cardinality += gm_weights[i]
      num_tracks = int(round(cardinality))
      if num_tracks > 1 or (track_id == 0 and num_tracks > 0):
        # Round the expected cardinality.
        # If it is greater than 1 or the track ID is 0 (which means it comes
        # from birth PHD), form new tracks
        gm_weights, gm_means, gm_covs = self._gm_assign_cluster_id(
          gm_weights=gm_weights,
          gm_means=gm_means,
          gm_covs=gm_covs,
          track_id=track_id,
          num_tracks=num_tracks,
          weight_threshold=cardinality/num_tracks
        )
    return gm_weights, gm_means, gm_covs

  def _gm_assign_cluster_id(self, gm_weights, gm_means, gm_covs,
                            track_id, num_tracks, weight_threshold,
                            max_iteration=1000):
    """Assign ID to a cluster of gaussian mixtures

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
    """
    num_gc = gm_weights.shape[0]
    # Reconstruct index array
    index_array = []
    for i in range(num_gc):
      if int(round(gm_means[i, -1])) == track_id:
        index_array.append(i)
    # Start cluster procedure
    cluster_means = np.random.uniform(0, 1, (num_tracks, self.n_visible))
    prev_cluster_means = None
    cluster_assignments = [[] for i in range(num_tracks)]
    iteration = 0
    while np.any(cluster_means != prev_cluster_means) and \
        iteration < max_iteration:
      prev_cluster_means = np.copy(cluster_means)
      # Calculate the distance between each Gaussian components (of the current
      # track) and the cluster mean, and arg_sort from high to low
      distance_matrix = self._gm_cluster_distance(
        gm_means, index_array, cluster_means
      )
      # Update cluster assignment
      cluster_assignments = self._gm_cluster_assignment_update(
        index_array=index_array,
        gm_weights=gm_weights,
        distance_matrix=distance_matrix,
        weight_threshold=weight_threshold
      )
      # Calculate mean to the clusters again
      for j in range(num_tracks):
        cluster_mean = np.zeros((self.n_visible,), dtype=np.float)
        cluster_weight = 0.
        for i in cluster_assignments[j]:
          cluster_mean += gm_means[i, :self.n_visible] * gm_weights[i]
          cluster_weight += gm_weights[i]
        if cluster_weight == 0.:
          cluster_weight = 1.
        cluster_means[j, :] = cluster_mean / cluster_weight
      # Update iteration
      iteration += 1
    # Out of the loop, assign ID to each track
    for j in range(num_tracks):
      # For every new track, start counting with max_id
      if track_id == 0 and j == 0:
        for i in cluster_assignments[j]:
          gm_means[i, -1] = self.next_track_id
        self.next_track_id += 1
      elif j != 0:
        for i in cluster_assignments[j]:
          gm_means[i, -1] = self.next_track_id
        self.next_track_id += 1
    # Return updated weights, means, and covs
    return gm_weights, gm_means, gm_covs

  def _gm_cluster_distance(self, gm_means, index_array, cluster_means):
    """Compute the distance between each Gaussian component and the center of
    each cluster.

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims+1]`
      index_array: List of Gaussian component indices that belongs to current
        track.
      cluster_means: Mean vector of each cluster, of shape
        `[num_clusters, n_visible]`

    Returns:
      An array of shape `[num_track_gc, num_clusters]` where each element in
      the array represents the distance of a Gaussian component to the cluster
    """
    num_track_gc = len(index_array)
    num_clusters = cluster_means.shape[0]
    gm_distance_array = np.zeros((num_track_gc, num_clusters), dtype=np.float)
    for i, gc_index in enumerate(index_array):
      for j in range(num_clusters):
        gm_distance_array[i, j] = distance.euclidean(
          gm_means[gc_index, :self.n_visible], cluster_means[j, :]
        )
    return gm_distance_array

  def _gm_cluster_assignment_update(self,
                                    index_array,
                                    gm_weights,
                                    distance_matrix,
                                    weight_threshold):
    """Based on the updated distance of each Gaussian component and center of
    cluster, update the group assignment of each Gaussian component.

    Args:
      index_array: List of Gaussian component indices that belongs to current
        track.
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      distance_matrix: Distance matrix between Gaussian components and cluster
        centers, of shape `[num_track_gc, num_clusters]`.

    Returns:
      New cluster assignment, a list of size `num_clusters` where each element
      is a list containing the index to the Gaussian component assigned to the
      cluster.
    """
    num_gc = len(index_array)
    # Notice that index `i` in this function refers to relative index of the
    # `index_array`. In order to get the actual index of corresponding
    # Gaussian component, it needs to be translated.
    indices_to_assign = set(range(num_gc))
    num_clusters = distance_matrix.shape[1]
    cluster_weights = np.zeros((num_clusters,), dtype=np.float)
    sorted_cluster_indexes = np.argsort(distance_matrix, axis=1)
    choice = 0
    # Clusters with indices to `index_array`
    cluster_indices = [[] for j in range(num_clusters)]
    # Assign gms to each cluster based on the distance between the center of
    # each cluster and the mean of each Gaussian components with the constraint
    # that the total weight of each cluster does not exceed weight_threshold.
    while len(indices_to_assign) > 0 and choice < num_clusters:
      temp_clusters = [[] for j in range(num_clusters)]
      # 1. Assign each Gaussian mixture to their closest cluster
      for i in indices_to_assign:
        temp_clusters[sorted_cluster_indexes[i, choice]].append(i)
      # 2. Make sure that the weight of each cluster does not exceed the
      #    maximum defined by `orig_track_weights/num_tracks`.
      for j in range(num_clusters):
        # a) We first sort the Gaussian mixture based on their distance to the
        #    center of the cluster
        temp_clusters[j] = sorted(
          temp_clusters[j],
          key=lambda i: distance_matrix[i, j]
        )
        # b) Compute the total weight. Keep the first `k` Guassian components
        #    until the weight exceeds threshold
        for i in temp_clusters[j]:
          cluster_weights[j] += gm_weights[index_array[i]]
          if cluster_weights[j] < weight_threshold:
            indices_to_assign.remove(i)
            cluster_indices[j].append(i)
          else:
            cluster_weights[j] -= gm_weights[index_array[i]]
            break
      # Choose the next-likely choice
      choice += 1
    # Certainly, it cannot guarantee that all Gaussian components are assigned
    # to a certain cluster. As a result, we need to take care of the rest of
    # Gaussian components that are left behind and assign them properly.
    # Ideally, they should be assigned according to distance between center of
    # the cluster and the Gaussian component.
    # However, a simple approximation can be used to assign the rest of
    # Gaussian components to the nearest cluster while the total weight of
    # that cluster is smaller than 1..
    for i in indices_to_assign:
      for choice in range(num_clusters):
        j = sorted_cluster_indexes[i, choice]
        cluster_weights[j] += gm_weights[index_array[i]]
        if cluster_weights[j] < 1.5:
          cluster_indices[j].append(i)
        else:
          cluster_weights[j] -= gm_weights[index_array[i]]
          break
    # Return cluster assignments
    return [
      [index_array[i] for i in cluster_indices[j]]
      for j in range(num_clusters)
    ]

  def observation_association(self,
                              gm_weights,
                              gm_means,
                              gm_covs,
                              observations):
    """Associate each sensor observation with existing track ID.

    Track to observation association is executed by maximizing the likelihood
    of probability of getting the current measurement based on the posterior
    PHD.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.

    Returns:
      a matrix of shape `[num_obs,]` populated with the track ID of the
      corresponding observation.
    """
    n_gc = gm_weights.shape[0]
    n_obs = observations.shape[0]
    # Group Gaussian components corresponding to a certain track
    track_gc_dict = {}
    for i in range(n_gc):
      track = int(round(gm_means[i, -1]))
      if track in track_gc_dict:
        track_gc_dict[track].append(i)
      else:
        track_gc_dict[track] = [i]
    track_list = list(track_gc_dict.keys())
    n_tracks = len(track_list)
    # Calculating the probability density of each observations
    obs_track_prob = np.zeros((n_obs, n_tracks), dtype=np.float)
    for rid in range(n_tracks):
      r = track_list[rid]
      for j in range(n_obs):
        for i in track_gc_dict[r]:
          obs_track_prob[j, rid] += scipy.stats.multivariate_normal.pdf(
            x=observations[j, :],
            mean=gm_means[i, :self.n_visible],
            cov=gm_covs[i, :self.n_visible, :self.n_visible]
          )
    # Find observation to track association
    ota = np.zeros((n_obs,), dtype=np.int)
    for j in range(n_obs):
      ota[j] = track_list[np.argmax(obs_track_prob[j, :])]
    return ota

  def generate_synthetic_data(self,
                              target_birth_steps,
                              target_birth_locations,
                              target_death_steps,
                              simulation_length,
                              clutter_spatial_sample_fn,
                              bounds=None):
    """Generate synthetic data based on the model specified.

    Args:
      target_birth_steps: The time step of the birth of each target.
      target_birth_locations: The spacial location of the birth of each target.
      target_death_steps: The time step of the death of each target.
      simulation_length: The total length of the generated synthetic data.
      clutter_spatial_sample_fn: The function callback to generate noisy
        observations.
      bounds: A list of boundary values specifying the boundary of the world.
        If the location of a target exceeds the bound, it will enter from the
        opposite side of the space. The boundary value is ordered as a numpy
        array of shape `[n_visible, 2]` where the first column is the lower
        boundary and the second column is the upper bound. The noise
        observations generated by the clutter process is not affected by the
        bounds.

    Returns:
      A two tuple of ground truth and noisy observations. The ground truth is
      a dictionary object containing a list of target locations (key
      "locations"), and the corresponding target ID (key "targets"). The
      observation is a list of numpy array each details the sensor observations
      at corresponding time step.
    """
    # Get total number of targets
    num_targets = len(target_birth_steps)
    # Check if birth locations include the target ID field. If not, append it.
    if target_birth_locations.shape[1] != self.ndims:
      target_birth_locations = np.block([
        target_birth_locations, np.arange(num_targets).reshape((num_targets, 1))
      ])
    return super(LinearMTGM_ID_Model, self).generate_synthetic_data(
      target_birth_steps=target_birth_steps,
      target_birth_locations=target_birth_locations,
      target_death_steps=target_death_steps,
      simulation_length=simulation_length,
      clutter_spatial_sample_fn=clutter_spatial_sample_fn,
      bounds=bounds
    )

  def _synthesize_state(self, state):
    """Synthesize the next state of the target.

    Args:
      state: previous state of the target, of shape `[ndims,]`.

    Returns:
      target state at next time step, of shape `[ndims,]`.
    """
    next_state = np.matmul(self.F, state) + np.block([
      np.random.multivariate_normal(
        mean=np.zeros((self.ndims-1, ), dtype=np.float),
        cov=self.Q[:self.ndims-1, :self.ndims-1]
      ),
      0
    ])
    return next_state

  def call(self, inputs, prunning=None):
    """GM-PHD Forward Pass

    Based on the posterior multi-target PHD (in the form of Gaussian Mixture)
    of previous time step (after the corrector), predict the multi-target PHD
    of current time step and correct the prediction based on the current
    observations.

    - gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
    - gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`
    - gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
    - observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.

    Args:
      inputs: A list of tensors encodes the weights, means and covariance
        matrix of PHD in the form of a Gaussian mixture and the observations at
        current time step
      prunning: Whether the Gaussian mixture is prunned according to a merge
        threshold and maximum size of Gaussian components.

    Returns:
      A three tuple composed of weights, means and covariance matrices of the
      posterior multi-target PHD of current time step in the form of Gaussian
      mixture.
    """
    # Acquire the variables for GM-PHD of previous time step
    gm_weights = inputs[0]
    gm_means = inputs[1]
    gm_covs = inputs[2]
    # Update means and covs dimensions
    n_gc = gm_weights.shape[0]
    if gm_means.shape[1] != self.ndims:
      gm_means = np.block([gm_means, np.ones((n_gc, 1), dtype=np.float)])
    if gm_covs.shape[1] != self.ndims:
      gm_covs = np.block([
        [gm_covs, np.zeros((n_gc, self.ndims - 1, 1))],
        [np.zeros((n_gc, 1, self.ndims - 1)), np.ones((n_gc, 1, 1))]
      ])
    # Call super
    return super(LinearMTGM_ID_Model, self).call((
      gm_weights, gm_means, gm_covs, inputs[3]
    ))


class CvMTGM_ID_Model(LinearMTGM_ID_Model):
  """Constant Velocity Multi-target Gaussian Mixture Model with Track ID

  Args:
    n_visible: Dimension of visible state.
    v_stddev: Standard deviation of velocity.
    m_stddev: Standard deviation of sensor measurement.
    birth: A weights, means and covariance matrices three-tuple defining the
      Gaussian components of birth PHD.
    p_d: Detection probability.
    p_s: Survival probability.
    clutter_lam: Lambda of Poisson distribution.
    clutter_c: Uniform probability density of the clutter process.
  """
  def __init__(self,
               n_visible,
               v_stddev,
               m_stddev,
               birth,
               p_d=0.90,
               p_s=0.99,
               clutter_lam=1.,
               clutter_c=0.01):
    F = np.block([[
      np.eye(n_visible, dtype=np.float),
      np.eye(n_visible, dtype=np.float)
    ],[
      np.zeros((n_visible, n_visible), dtype=np.float),
      np.eye(n_visible, dtype=np.float)
    ]])
    G = np.block([
      [0.5 * np.eye(n_visible, dtype=np.float)],
      [np.eye(n_visible, dtype=np.float)]
    ])
    Q = np.matmul(
      G, np.matmul(np.eye(n_visible, dtype=np.float) * (v_stddev ** 2), G.T)
    )
    R = np.eye(n_visible, dtype=np.float) * (m_stddev ** 2)
    super(CvMTGM_ID_Model, self).__init__(
      n_hidden=n_visible,
      n_visible=n_visible,
      birth=birth,
      p_d=p_d,
      p_s=p_s,
      F=F,
      Q=Q,
      R=R,
      clutter_lam=clutter_lam,
      clutter_c=clutter_c
    )
