# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
"""Multiple Object Tracking Performance metrics test"""

import unittest
import numpy as np
from smrt.metrics.clear_metrics import mota_score
from smrt.metrics.clear_metrics import motp_score


class TestMultipleObjectTrackingMetrics(unittest.TestCase):

  def test_motaScore(self):
    truth_location = [
      np.array([[0., 0.],
                [1., 0.],
                [0., 1.]]),
      np.array([[0.1, 0.1],
                [1.1, 0.1],
                [0.1, 1.1]]),
      np.zeros((0, 2))
    ]
    prediction_location = [
      np.array([[0.1, 0.1],
                [1.1, 0.2],
                [1.1, 0.1],
                [0.1, 0.5],
                [0.5, 0.2]]),
      np.zeros((0, 2)),
      np.array([[0.1, 0.1],
                [0.2, 0.2]])
    ]
    truth_targets = [
      np.array([0, 1, 2]),
      np.array([1, 2, 3]),
      np.zeros((0,))
    ]
    truth = {
      "locations": truth_location,
      "targets": truth_targets
    }
    prediction = {
      "locations": prediction_location
    }
    print("MOTA Score: %.4f" % mota_score(truth, prediction, 0.3))
    print("MOTP Score: %.4f" % motp_score(truth, prediction, 0.3))


if __name__ == '__main__':
    unittest.main()
