# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
""""Multiple Object Tracking Performance metrics

This script implements the following Multiple Object Tracking performance
metrics:

- MODA: Multiple Object Detection Accuracy
- MODP: Multiple Object Detection Precision
- MOTA: Multiple Object Tracking Accuracy
- MOTP: Multiple Object Tracking Precision

Reference:
  1. Kasturi, Rangachar, Dmitry Goldgof, Padmanabhan Soundararajan,
  Vasant Manohar, John Garofolo, Rachel Bowers, Matthew Boonstra,
  Valentina Korzhova, and Jing Zhang. "Framework for performance evaluation of
  face, text, and vehicle detection and tracking in video: Data, metrics, and
  protocol." IEEE Transactions on Pattern Analysis and Machine Intelligence 31,
  no. 2 (2009): 319-336.
  2. Bernardin, Keni, Alexander Elbs, and Rainer Stiefelhagen. "Multiple object
  tracking performance metrics and evaluation in a smart room environment."
  Sixth IEEE International Workshop on Visual Surveillance, in conjunction
  with ECCV. Vol. 90. 2006.
"""
import numpy as np
from scipy.optimize import linear_sum_assignment
from scipy.spatial import distance
from tensorflow.python.keras.utils.generic_utils import Progbar


def mot_hypothesis_association(truth, prediction, ndims=None, threshold=None):
  """Associate tracking hypothesis with the ground truth labels so that misses,
  false positives and mismatches can be computed.

  However, in different literature, the rules used for hypothesis association
  vary. In [1], the association is computed globally using Hungarian algorithm
  at each time step. In [2], if the existing hypothesis is still within a
  certain threshold, the hypothesis association will be maintained.

  Depending on the application, the metric score for every combination of ground
  truth and system output pair are calculated may be different. In case of a
  point target, the metric score is the distance between ground truth and the
  predicted location of a target in the state space. In case of MOT in video,
  the spatial overlap between ground truth objects and system object tracks is
  used.

  In this implementation, we consider the target is a point target. The
  Euclidean distance is used to measure the difference between truth and
  prediction.

  Args:
    truth: Dictionary with `locations` key and `targets` key. The locations
      contain the a list of targets locations, and the targets contain the
      corresponding target ID. If the target ID is embedded in the state vector,
      the last dimension of the state vector is the target ID.
    prediction: Dictionary with `locations` key and `targets` key.
    ndims: Number of dimensions to use for distance calculation. Use all if
      `None`.
    threshold: The threshold value to maintain truth to hypothesis association.

  Returns:
    List of associations ordered by time steps. The associations of each time
    step is a list of dictionaries, with keys `truth_id`, `truth_location`,
    `prediction_id` and `prediction_location`. If a prediction is a false
    positive (i.e. not associated with a truth), the `truth_id` is set to None.
    If a truth is a miss (i.e. not associated with a prediction), the
    `target_id` is set to None.
  """
  n_steps = len(truth["locations"])
  association_results = []
  current_mapping = {}
  for k in range(n_steps):
    associations = []
    truth_to_associate = {
      'id': [],
      'locations': []
    }
    prediction_to_associate = {
      'id': [],
      'locations': []
    }
    truth_locations = _assemble_location_by_id(
      truth['locations'][k], truth['targets'][k], ndims=ndims
    )
    prediction_locations = _assemble_location_by_id(
      prediction['locations'][k], prediction['targets'][k], ndims=ndims
    )
    # Default, including everything
    truth_to_associate['id'] = list(truth['targets'][k])
    prediction_to_associate['id'] = list(prediction['targets'][k])
    if ndims is None:
      truth_to_associate['locations'] = [
        truth['locations'][k][i, :]
        for i, tid in enumerate(truth_to_associate['id'])
      ]
      prediction_to_associate['locations'] = [
        prediction['locations'][k][i, :]
        for i, tid in enumerate(prediction_to_associate['id'])
      ]
    else:
      truth_to_associate['locations'] = [
        truth['locations'][k][i, :ndims]
        for i, tid in enumerate(truth_to_associate['id'])
      ]
      prediction_to_associate['locations'] = [
        prediction['locations'][k][i, :ndims]
        for i, tid in enumerate(prediction_to_associate['id'])
      ]
    # If threshold is provided, prioritize exsiting associations.
    if threshold is not None:
      # Consider if existing associations are still valid, before binary
      # assignment with Hungarian method.
      for truth_id in current_mapping:
        prediction_id = current_mapping[truth_id]
        if prediction_id in prediction_locations and \
            truth_id in truth_locations:
          association_distance = distance.euclidean(
            prediction_locations[prediction_id], truth_locations[truth_id]
          )
          if association_distance < threshold:
            t_id = truth_to_associate['id'].index(truth_id)
            p_id = prediction_to_associate['id'].index(prediction_id)
            associations.append({
              'truth_id': truth_to_associate['id'][t_id],
              'truth_location': truth_to_associate['locations'][t_id],
              'prediction_id': prediction_to_associate['id'][p_id],
              'prediction_location': prediction_to_associate['locations'][p_id]
            })
            del truth_to_associate['id'][t_id]
            del truth_to_associate['locations'][t_id]
            del prediction_to_associate['id'][p_id]
            del prediction_to_associate['locations'][p_id]
    # Form association with Hungarian method
    num_truth = len(truth_to_associate['id'])
    num_prediction = len(prediction_to_associate['id'])
    cost_matrix = np.zeros((num_truth, num_prediction), dtype=np.float)
    for i, truth_id in enumerate(truth_to_associate['id']):
      for j, pred_id in enumerate(prediction_to_associate['id']):
        cost_matrix[i, j] = distance.euclidean(
          truth_to_associate['locations'][i],
          prediction_to_associate['locations'][j]
        )
    truth_ind, pred_ind = linear_sum_assignment(cost_matrix)
    pred_total_inds = set(range(num_prediction))
    truth_total_inds = set(range(num_truth))
    # Add the association
    for p_id, t_id in zip(pred_ind, truth_ind):
      associations.append({
        'truth_id': truth_to_associate['id'][t_id],
        'truth_location': truth_to_associate['locations'][t_id],
        'prediction_id': prediction_to_associate['id'][p_id],
        'prediction_location': prediction_to_associate['locations'][p_id]
      })
      pred_total_inds.remove(p_id)
      truth_total_inds.remove(t_id)
    # Update current mapping
    for association in associations:
      current_mapping[association['truth_id']] = association['prediction_id']
    # Add misses or false positives
    for t_id in truth_total_inds:
      associations.append({
        'truth_id': truth_to_associate['id'][t_id],
        'truth_location': truth_to_associate['locations'][t_id],
        'prediction_id': None,
        'prediction_location': None
      })
      current_mapping[truth_to_associate['id'][t_id]] = None
    for p_id in pred_total_inds:
      associations.append({
        'truth_id': None,
        'truth_location': None,
        'prediction_id': prediction_to_associate['id'][p_id],
        'prediction_location': prediction_to_associate['locations'][p_id]
      })
    association_results.append(associations)
  return association_results


def mota_report(associations):
  """Calculate the Multiple Object Tracking Accuracy (MOTA) score

  MOTA = 1 - \frac{c(m) + c(fp) + c(mme)}{N_G}

  In the equation above, `m` is the number of misses, `fp` is the number of
  false positives, and `mme` is the number of mismatches, occurred when the
  track identifier generated by the system is changed.

  The function only requires that it is sufficient to determine misses, false
  positives and ID mismatches.

  Args:
    associations: List of associations ordered by time steps. The associations
    of each time step is a list of dictionaries, with keys `truth_id`,
    `truth_location`, `prediction_id` and `prediction_location`. If a prediction
    is a false positive (i.e. not associated with a truth), the `truth_id` is
    set to None. If a truth is a miss (i.e. not associated with a prediction),
    the `target_id` is set to None.

  Returns:
    Dictionary containing details of MOTA errors. The dictionary is indexed by
    `total`, `correct`, `miss`, `fp`, `mismatch`.
  """
  n_steps = len(associations)
  # Temporary associations
  current_association = {}
  # results
  num_misses = 0
  num_false_positives = 0
  num_mismatches = 0
  num_correct = 0
  num_associations = 0
  # Start computing
  progbar = Progbar(
    target=n_steps,
    verbose=1,
    stateful_metrics=['total', 'correct', 'miss', 'fp', 'mismatch'],
    unit_name='step'
  )
  for k, associations_k in enumerate(associations):
    for association in associations_k:
      truth_id = association['truth_id']
      target_id = association['prediction_id']
      if truth_id is None:
        # False positive
        num_false_positives += 1
      else:
        num_associations += 1
        if target_id is None:
          num_misses += 1
        else:
          # Check if mismatch
          if truth_id in current_association:
            if current_association[truth_id] != target_id:
              num_mismatches += 1
              current_association[truth_id] = target_id
            else:
              num_correct += 1
          else:
            num_correct += 1
            current_association[truth_id] = target_id
    progbar.update(k + 1, [
      ('total', num_associations),
      ('correct', num_correct),
      ('miss', num_misses),
      ('fp', num_false_positives),
      ('mismatch', num_mismatches)
    ])
  return {
    'total': num_associations,
    'correct': num_correct,
    'miss': num_misses,
    'fp': num_false_positives,
    'mismatch': num_mismatches
  }


def motp_score(associations):
  """Calculate the Multiple Object Tracking Precision (MOTP) score

  MOTP = \frac{\sum d_{i, t}}{\sum c_t}

  In the above equation, $d_{i, t}$ is the distance between predicted target
  location and the ground truth state. $c_t$ is the number of matches found at
  time $t$.
  """
  M = mot_mapping_no_tid(truth, prediction, threshold)
  n_steps = len(truth["locations"])
  c_sum = 0
  d_sum = 0.
  for t in range(n_steps):
    match = M[t]
    c_sum += len(match)
    for j in range(len(match)):
      d_sum += np.linalg.norm(
        match[j]["truth_loc"] - match[j]["prediction_loc"]
      )
  return d_sum / c_sum

def mota_score(associations):
  """Calculate the Multiple Object Tracking Accuracy (MOTA) score

  MOTA = 1 - \frac{c(m) + c(fp) + c(mme)}{N_G}

  In the equation above, `m` is the number of misses, `fp` is the number of
  false positives, and `mme` is the number of mismatches, occurred when the
  track identifier generated by the system is changed.

  The function only requires that it is sufficient to determine misses, false
  positives and ID mismatches.

  Args:
    associations: List of associations ordered by time steps. The associations
    of each time step is a list of dictionaries, with keys `truth_id`,
    `truth_location`, `prediction_id` and `prediction_location`. If a prediction
    is a false positive (i.e. not associated with a truth), the `truth_id` is
    set to None. If a truth is a miss (i.e. not associated with a prediction),
    the `target_id` is set to None.
  """
  mota_result = mota_report(associations)
  return 1 - (
    mota_result['miss'] + mota_result['fp'] + mota_result['mismatch']
  ) / mota_result['total']


def _assemble_location_by_id(locations, targets, ndims=None):
  """Assemble the locations and target identifiers list into a dictionary
  indexed by the target identifiers.

  Args:
    locations: numpy array of shape `[num_targets, ndims]`, representing the
      state vectors of the targets in either ground truth or predictions.
    targets: numpy array of shape `[num_targets,]`, representing the
      corresponding target identifiers.

  Returns:
    A dictionary indexed by target identifier, with value equal to the state
    vector of corresponding targets.
  """
  location_dict = {}
  num_targets = np.array(targets).shape[0]
  for i in range(num_targets):
    tid = targets[i]
    if ndims is None:
      location_dict[tid] = locations[i, :]
    else:
      location_dict[tid] = locations[i, :ndims]
  return location_dict


def mot_mapping_no_tid(truth, prediction, threshold, n_visible=2):
  """Mapping procedure between truth and prediction without track ID in
  prediction.
  """
  n_steps = len(truth["locations"])
  M = []
  for t in range(n_steps):
    locations_truth = truth["locations"][t]
    targets_truth = truth["targets"][t]
    locations_pred = prediction["locations"][t]
    n_targets_truth = locations_truth.shape[0]
    n_targets_pred = locations_pred.shape[0]
    current_M = []
    unmapped_index_pred = set(range(n_targets_pred))
    unmapped_index_truth = set(range(n_targets_truth))
    # Find matching correspondence
    for i in range(n_targets_truth):
      if n_targets_pred > len(current_M):
        target_location = locations_truth[i, :n_visible]
        distance_list = np.linalg.norm(
          locations_pred[:, :n_visible] - target_location, axis=1
        )
        # Sorted: Low to High
        sorted_indices = np.argsort(distance_list)
        for j in sorted_indices:
          if j in unmapped_index_pred and distance_list[j] <= threshold:
            current_M.append({
              "truth_loc": target_location,
              "truth_id": targets_truth[i],
              "prediction_loc": locations_pred[j, :n_visible],
              "prediction_id": targets_truth[i]
            })
            unmapped_index_pred.remove(j)
            unmapped_index_truth.remove(i)
            break
      else:
        break
    M.append(current_M)
  return M

