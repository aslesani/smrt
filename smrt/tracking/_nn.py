###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""Multi-resident tracking using Nearest Neighbor algorithm with Sensor Graph
"""
import numpy as np
from tensorflow.python.keras.utils.generic_utils import Progbar


def nn_sg(sequence, sensor_graph, adjacency_matrix, max_undetected_period=50):
  """ Nearest neighbor with sensor graph for multi-resident tracking

  Args:
    sequence: two tuple composed of the sensor sequence and the corresponding
      time tags.
    sensor_graph: A matrix of shape `[num_sensors, num_sensors]`, representing
      the conditional probability of any resident moving from sensor $i$ to
      sensor $j$, where $i$ is the row index, and $j$ is the column index.
    adjacency_matrix: A symmetric matrix of shape `[num_sensors, num_sensors]`,
      representing the adjacency between two sensors. Two sensors are adjacent
      if a resident can trigger both sensor consecutively without activating
      any other sensor in the smart home.
    max_undetected_period: The maximum number of time steps that a target could
      remain undetected by any sensor in the smart home without being assumed to
      have left the house or become inactive.

  Returns:
    List of associations between sensor event and identified targets. Each
    association is a dictionary with the following keys:
    - start: time tag of the sensor event in the sequence
    - sensor: sensor ID
    - tracks: list of track ID. In the case of NN-sg, it only contains one
      element.
  """
  # Nearest Neighbor with Sensor Graph
  sensor_seq, seq_timetags = sequence

  # Temporary variables
  num_targets = 0
  active_targets = set()
  targets_location = {}
  targets_last_active = {}

  # We use keras progress bar to track the progress
  progbar = Progbar(
    target=len(sensor_seq),
    verbose=1,
    stateful_metrics=['active targets', 'total targets'],
    unit_name='step'
  )

  # Sensor event to resident association.
  sensor_event_association = []

  # The result holds the association between each sensor event
  # and the tracking targets
  for i, sensor in enumerate(sensor_seq):
    # Find previous track
    possible_targets = []
    possible_association_likelihood = []
    # Track each active targets
    for target in active_targets:
      prev_location = targets_location[target]
      if adjacency_matrix[prev_location, sensor]:
        possible_targets.append(target)
        possible_association_likelihood.append(
          sensor_graph[prev_location, sensor]
        )
    # Now, if there is any possible association, get the one with the highest likelihood
    if len(possible_targets) > 0:
      tid = possible_targets[np.argmax(possible_association_likelihood)]
      # Book-keeping
      targets_location[tid] = sensor
      targets_last_active[tid] = i
    else:
      # No exsiting target found, create new entity
      num_targets += 1
      tid = num_targets
      targets_location[tid] = sensor
      targets_last_active[tid] = i
      active_targets.add(tid)
    # Add association to the result
    sensor_event_association.append({
      'start': seq_timetags[i],
      'sensor': sensor,
      'tracks': [tid]
    })
    # Targets that has not been detected over certain period of time is considered
    # to have left the house or become inactave. Remove them from the list
    targets_to_remove = []
    for target in active_targets:
      if (i - targets_last_active[target]) >= max_undetected_period:
        targets_to_remove.append(target)
    for target in targets_to_remove:
      active_targets.remove(target)
    # Report progress
    progbar.update(i + 1, [
      ('active targets', len(active_targets)),
      ('total targets', num_targets)
    ])
  # Return
  return sensor_event_association
