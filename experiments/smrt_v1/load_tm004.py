###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""Part I: Load TM004 Dataset
(SMRT v1.0 Experiment Script)

This script downloads TM004 dataset and decompressed it into `data` directory.
"""

import os
import logging
import pickle
import urllib.request
import zipfile
from pycasas.data import CASASDataset


def config_debug():
  logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
    handlers=[logging.StreamHandler()])


dataset_url = 'https://eecs.wsu.edu/~twang3/datasets/'
dataset_filename = 'tm004_161219_9d.zip'
dataset_local_path = os.path.join(
  os.path.dirname(__file__), '..', '..', 'data', 'casas'
)
chkpt_dir = os.path.join(
  os.path.dirname(__file__), 'data'
)


def download_dataset(url, filename):
  """Download dataset file if not present.
  """
  local_filename = os.path.join(dataset_local_path, filename)
  if not os.path.exists(local_filename):
    local_filename, _ = urllib.request.urlretrieve(
      url + filename, local_filename
    )

def read_dataset(filename):
  """Extract dataset from downloaded zip file"""
  if not os.path.isdir(
    os.path.join(dataset_local_path, os.path.splitext(filename)[0])
  ):
    local_filename = os.path.join(dataset_local_path, filename)
    with zipfile.ZipFile(local_filename) as f:
      f.extractall(dataset_local_path)

def load_dataset(dataset_dir):
  """Load dataset from folder and save """
  dataset = CASASDataset(directory=dataset_dir)
  dataset_chkpt_file = os.path.join(chkpt_dir,
                                    dataset.data_dict['name'] + ".pkl")
  if os.path.exists(dataset_chkpt_file):
    dataset = CASASDataset.load(dataset_chkpt_file)
  else:
    dataset.load_events(show_progress=True)
    dataset.save(dataset_chkpt_file)
  dataset.summary()
  return dataset

def get_sequence(dataset):
  sequence_chkpt_file = os.path.join(
    chkpt_dir,
    dataset.data_dict['name'] + "_sequence.pkl"
  )
  if os.path.exists(sequence_chkpt_file):
    fp = open(sequence_chkpt_file, "rb")
    sequence = pickle.load(fp)
    fp.close()
  else:
    sequence = dataset.to_sensor_sequence()
    fp = open(sequence_chkpt_file, "wb")
    pickle.dump(sequence, fp, protocol=-1)
    fp.close()
  return sequence

def get_observations(dataset):
  observation_chkpt_file = os.path.join(
    chkpt_dir,
    dataset.data_dict['name'] + "_observations.pkl"
  )
  if os.path.exists(observation_chkpt_file):
    fp = open(observation_chkpt_file, "rb")
    observations = pickle.load(fp)
    fp.close()
  else:
    observations = dataset.to_observation_track()
    fp = open(observation_chkpt_file, "wb")
    pickle.dump(observations, fp, protocol=-1)
    fp.close()
  return observations


if __name__ == "__main__":
  config_debug()
  os.makedirs(chkpt_dir, exist_ok=True)
  # Download dataset if it does not exists
  download_dataset(dataset_url, dataset_filename)
  read_dataset(dataset_filename)
  # Load dataset
  dataset = load_dataset(dataset_dir=os.path.join(
    dataset_local_path, os.path.splitext(dataset_filename)[0]
  ))
  # Get sequence and observation
  sequence = get_sequence(dataset)
  observations = get_observations(dataset)
  # Check sensor events in this dataset
  dataset.check_event_list(os.path.join(
      chkpt_dir, dataset.get_name() + '_event_check.xlsx'
  ))
