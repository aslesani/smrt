###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""Part II-B: Vector Embeddings of sensors in TM004 Dataset
(SMRT v1.0 Experiment Script)

This script computes the vector embeddings of sensors in TM004 dataset with
various hyperparameter settings. The skip window size rages from 1 - 17.
The dimensionality of measurement ranges from 2 to 6.
Batch size is fixed to 128, negative samples for NCE loss is fixed at 14.
We run 10 iterations for each hyper parameter sets.
"""

import os
from subprocess import Popen

script_name = "svec_tm004.py"

# Run 10 trials for each combination
iterations = 10

# Parameter choices
# skip_window: number of sensors to consider left and right (1, 3, 7, 11, 17)
# num_skips: how amny times to reuse an input to generate a pair
#   default to 2 * skip_window
# embedding_size: the size of vector embedding representation for sensors.
#   default to 3, can consider 2, 4, 5, 6
skip_window_choices = [1, 3, 7, 11, 17]
num_skips_choices = [2 * skip_window for skip_window in skip_window_choices]
embedding_size_choices = [2, 3, 4, 5, 6]

# Training parameters - fixed. They may not have high variation to
# batch_size: fix to 128
# num_epochs: fix to 50
# num_sampled: number of negative samples in NCE loss.
#   default to 50% of total number of sensors in the smart home
batch_size = 128
num_epochs = 10
num_sampled = 14


if __name__ == "__main__":
  for skip_window, num_skips in zip(skip_window_choices, num_skips_choices):
    for embedding_size in embedding_size_choices:
      print("\n\r=== Start Embedding ===")
      print("- skip_window: %d" % skip_window)
      print("- num_skips: %d" % num_skips)
      print("- embedding_size: %d" % embedding_size)
      for i in range(iterations):
        cmd = ["python",
               os.path.join(os.path.dirname(__file__), script_name),
               "-nn", str(num_sampled),
               "-bs", str(batch_size),
               "-e", str(num_epochs),
               "-sw", str(skip_window),
               "-ns", str(num_skips),
               "-pf", "t%d" % i,
               "-s", str(embedding_size)
               ]
        print("Trial %d: %s" % (i, " ".join(cmd)))
        p = Popen(cmd)
        rtn = p.wait()
