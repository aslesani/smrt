###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""Part II-A: Vector Embeddings of sensors in TM004 Dataset
(SMRT v1.0 Experiment Script)

This script computes the vector embeddings of sensors in TM004 dataset with
configurable hyperparameters. This script takes those hyperparameters as
command line arguments.
"""

import os
import argparse
from datetime import datetime
import logging
import pickle
import random
import numpy as np
import tensorflow as tf
import tensorflow.python.keras as keras
from pycasas.data import CASASDataset
from smrt.learning.models.embedding import NCEModel
from smrt.utils import normalize_embeddings

def config_debug():
  logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
    handlers=[logging.StreamHandler()])


data_chkpt_dir = os.path.join(
  os.path.dirname(__file__), 'data'
)
dataset_pickle_filename = os.path.join(
  data_chkpt_dir, 'tm004_161219_9d.pkl'
)
dataset_observations_filename = os.path.join(
  data_chkpt_dir, 'tm004_161219_9d_observations.pkl'
)
dataset_sequence_filename = os.path.join(
  data_chkpt_dir, 'tm004_161219_9d_sequence.pkl'
)
embeddings_chkpt_dir = os.path.join(
  os.path.dirname(__file__), 'embeddings'
)
log_dir = os.path.join(
  os.path.dirname(__file__), 'logs'
)
load_script = 'load_tm004.py'
dataset_prefix = "tm004"
embedding_dir_template = "%s_ns_%d_sw_%d%s"
embeddings_filename_template = ""
embedding_postfix = ""

def load_dataset():
  """Load dataset from folder and save"""
  if os.path.exists(dataset_pickle_filename):
    dataset = CASASDataset.load(dataset_pickle_filename)
    dataset.summary()
    return dataset
  else:
    raise FileNotFoundError(
      "Failed to find pickled dataset file %s, "
      "please run %s first." %
      (dataset_pickle_filename, load_script)
    )

def get_sequence():
  if os.path.exists(dataset_sequence_filename):
    fp = open(dataset_sequence_filename, "rb")
    sequence = pickle.load(fp)
    fp.close()
    return sequence
  else:
    raise FileNotFoundError(
      "Failed to find pickled sequence file %s, "
      "please run %s first." %
      (dataset_sequence_filename, load_script)
    )

def get_observations():
  if os.path.exists(dataset_observations_filename):
    fp = open(dataset_observations_filename, "rb")
    observations = pickle.load(fp)
    fp.close()
    return observations
  else:
    raise FileNotFoundError(
      "Failed to find pickled observations file %s, "
      "please run %s first." %
      (dataset_sequence_filename, load_script)
    )

def generate_train_pair(data, num_skips, skip_window):
  """Generating training dataset using skip gram models
  """
  num_windows = len(data) - 2 * skip_window
  size = num_windows * num_skips
  source = np.zeros(shape=(size,), dtype=np.int32)
  target = np.zeros(shape=(size,), dtype=np.int32)
  span = 2 * skip_window + 1
  for i in range(num_windows):
    context_sensors = [w for w in range(span) if w != skip_window]
    sensors_to_use = random.sample(context_sensors, num_skips)
    for j, context_sensor in enumerate(sensors_to_use):
      source[i * num_skips + j] = data[i + skip_window]
      target[i * num_skips + j] = data[i + context_sensor]
  return source, target

def get_training_pair(data, num_skips, skip_window):
  """Generating training pairs
  """
  # Form training filename
  training_dir = os.path.join(
    embeddings_chkpt_dir,
    embedding_dir_template % (
      dataset_prefix, num_skips, skip_window,
      '' if embedding_postfix == "" else '_' + embedding_postfix
    )
  )
  embeddings_training_filename = os.path.join(
    training_dir, 'training.npz'
  )
  if os.path.exists(embeddings_training_filename):
    training_data = np.load(embeddings_training_filename)
    x = training_data['x']
    y = training_data['y']
  else:
    os.makedirs(training_dir, exist_ok=True)
    x, y = generate_train_pair(data, num_skips, skip_window)
    training_data = {
      'x': x, 'y': y
    }
    np.savez(embeddings_training_filename, **training_data)
  return x, y

def save_embeddings(
  embeddings, num_skips, skip_window,
  num_sampled, num_epochs, normalized=True
):
  """Save embeddings to npz file
  """
  # Form training filename
  embedding_dir = os.path.join(
    embeddings_chkpt_dir,
    embedding_dir_template % (
      dataset_prefix, num_skips, skip_window,
      '' if embedding_postfix == "" else '_' + embedding_postfix
    )
  )
  embeddings_training_filename = os.path.join(
    embedding_dir, 'embeddings%s_nn_%d_ne_%d_dim%d.npz' % (
      ('_normalized' if normalized else ''),
      num_sampled, num_epochs, embedding_size
    )
  )
  np.savez(embeddings_training_filename, embeddings)

skip_window = 1  # How many words to consider left and right.
num_skips = 2  # How many times to reuse an input to generate a label.
batch_size = 128 # Batch size
embedding_size = 3  # Dimension of the embedding vector.
num_sampled = 12  # Number of negative examples to sample.
num_epochs = 20 # Number of epochs to run NCE

def empty_loss(y_true, y_pred):
  return tf.constant(0.)

def parse_args():
  parser = argparse.ArgumentParser(
    description="Generate sensor embeddings for the smart home"
  )
  parser.add_argument("-nn", "--num-sampled", default=num_sampled, type=int,
                      help="Number of negative samples in NCE loss.")
  parser.add_argument("-bs", "--batch-size", default=batch_size, type=int,
                      help="Batch size, must be a multiple of the num_skips "
                           "size per window.")
  parser.add_argument("-sw", "--skip-window", default=skip_window, type=int,
                      help="Skip window size.")
  parser.add_argument("-ns", "--num-skips", default=num_skips, type=int,
                      help="Width per window.")
  parser.add_argument("-e", "--num-epochs", default=num_epochs,
                      type=int, help="Number of training epochs")
  parser.add_argument("-pf", "--postfix", default=embedding_postfix,
                      type=str, help="Postfix for embedding directory.")
  parser.add_argument("-s", "--embedding-size", default=embedding_size,
                      type=int, help="Embedding vector dimension, default to 3")
  parser.add_argument("-v", "--verbose", action="store_true",
                      help="Increase output verbosity.")
  return parser.parse_args()

if __name__ == "__main__":
  config_debug()
  os.makedirs(embeddings_chkpt_dir, exist_ok=True)
  os.makedirs(log_dir, exist_ok=True)

  args = parse_args()
  num_sampled = args.num_sampled
  batch_size = args.batch_size
  skip_window = args.skip_window
  num_epochs = args.num_epochs
  num_skips = args.num_skips
  embedding_postfix = args.postfix
  embedding_size = args.embedding_size

  dataset = load_dataset()
  sequence = get_sequence()
  observations = get_observations()
  x, y = get_training_pair(
    sequence[0], num_skips, skip_window
  )
  vocab_size = len(dataset.sensor_list)
  model = NCEModel(
    vocab_size=vocab_size,
    embeddings_size=embedding_size,
    nce_num_sampled=num_sampled
  )
  optimizer = keras.optimizers.RMSprop(
    lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0
  )
  model.compile(
    optimizer=optimizer,
    loss=empty_loss
  )

  # Prepare tensorboard callback
  logdir = os.path.join(log_dir, datetime.now().strftime("tm004_vec_%Y%m%d-%H%M%S"))
  tensorboard_callback = keras.callbacks.TensorBoard(logdir, profile_batch=0)
  os.makedirs(logdir, exist_ok=True)

  model.fit(
    x=[x.reshape([-1, 1]), y.reshape([-1, 1])],
    y=y,
    epochs=num_epochs,
    batch_size=64,
    callbacks=[tensorboard_callback]
  )

  sensor_x = np.arange(vocab_size)
  embeddings = model.predict((
    sensor_x.reshape([-1, 1]), sensor_x.reshape([-1, 1])
  ))
  normalized_embeddings = normalize_embeddings(embeddings)
  print(normalized_embeddings)
  save_embeddings(
    normalized_embeddings, num_skips, skip_window, num_sampled, num_epochs
  )
  save_embeddings(
    embeddings, num_skips, skip_window, num_sampled, num_epochs,
    normalized=False
  )
